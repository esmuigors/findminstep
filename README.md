# findminstep

A script to find the optimal step from a geometry (or SC-PCM) optimization run in the _Gaussian_ quantum chemistry program. If a cyclingf pattern is detected, tries to determine at which step does it start and then resweeps the part before it for the best step. Or maybe another range of cycling pattern.
The detection does not neccessarily work well at any time but ganerally works well enough to use it in automatic detection of cycling/aborting and restarting the optimization in our home-made wrapper for _Gaussian_ (which will also be published some day). Then the script runs in the low-output mode.


Usage:

    findminstep [OPTIONS] filename [filename2 filename3 ...]

Other options:

-  -s/--steps : printing of all steps
-  -b/--beststeps : printing of steps deemed best (usualy MORE than one)
-  -i INT/--strictness=INT : how strict to be when detecting cycling (smaller = stricter, may conclude that the whole optimization is rubbish; default = 3)
-  -n INT/ -normalize-to=INT : normalize convergence criteria to Normal (1, default), Tight (2, default if Opt=Tight is found within the job) or VeryTight (3, default if Opt=VeryTight is found within the job)
-  -x/--scpcm : find the best step of SC-PCM instead (x for eXtra)
-  -v/--verbose : same as -s (used for compatibility).

